function hello() {
    console.log('Hello', this);
}

const person = {
    name: 'Andrew',
    age: 25,
    sayHello: hello,
    sayHelloWindow: hello.bind(window),
    logInfo: function(job, phone) {
        console.group(`${this.name} info:`);
        console.log(`Name is ${this.name}, age is ${this.age}`);
        console.log(`Job is ${job}`);
        console.log(`Phone is ${phone}`);
        console.groupEnd();
    }
}
person.logInfo('Back-end', '+3809876543')

const lena = {
    name: 'Lena',
    age: 22
}

person.logInfo.bind(lena, 'Front-end', '+3801234567')()
person.logInfo.call(lena, 'Front-end', '+3801234567')
person.logInfo.apply(lena, ['Front-end', '+3801234567'])

const array = [1, 2, 3, 4, 5]

// function multyBy(arr, n) {
//     return arr.map(function(i) {
//         return i * n
//     })
// }

Array.prototype.multBy = function(n) {
    return this.map(function(i) {
        return i * n
    })
}

console.log(array.multBy(2));
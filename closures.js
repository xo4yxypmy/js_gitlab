function createCalcFunction(n) {
    return function() {
        console.log(1000 * n);
    }
}

const calc = createCalcFunction(10)
calc()

function createIncrementor(i) {
    return function(num) {
        return i + num
    }
}

const addOne = createIncrementor(1)
const addTen = createIncrementor(10)

console.log(addOne(10));
console.log(addTen(20));

function urlGenerator(domain) {
    return function(url) {
        return `https://${url}.${domain}`
    }
}

const comUrl = urlGenerator('com')

console.log(comUrl('google'));
console.log(comUrl('netflix'));

// -----------------

function logPerson() {
    console.log(`Person: ${this.name}, ${this.age}, ${this.job}`)
}

const person1 = { name: 'Andrew', age: 26, job: 'Front-end' }
const person2 = { name: 'John', age: 22, job: 'Back-end' }

function bind(context, fn) {
    return function(...args) {
        fn.apply(context, args)
    }
}

bind(person1, logPerson)()
bind(person2, logPerson)()